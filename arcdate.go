/*
Archive the configuraion documents according to the file modified date.
You can archive the documents to the specified YYYYMM folder
Or compress the documents to a zipped file
*/
package main

import (
	"archive/zip"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

//TODO: file pattern, date range, append
//TODO: use exitCode to make defer function executed
var pattern = flag.Int("p", 1, "Archive directory pattern: 1. YYYYMM; 2. YYYY, 3. YYMM")

func usage() {
	fmt.Fprintf(os.Stderr, "\nusage: "+os.Args[0]+" [-p=1] srcDir dstDir/dstName.zip\n\n")
	fmt.Fprintf(os.Stderr, "\nArchive the source folder files to the destination folder according to the modified date.\n\n")
	fmt.Fprintf(os.Stderr, "src is the name of the documents folder.\n"+
		"dst can be a folder or a zip file name.\n\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func check(err error) {
	if err != nil {
		log.Fatalln(os.Args[0] + " Error: " + err.Error())
	}
}

func dateFmt(n int) string {
	var p string

	switch n {
	case 1:
		p = "200601"
	case 2:
		p = "2006"
	case 3:
		p = "0601"
	default:
		usage()
	}
	return p
}

func main() {
	flag.Usage = usage
	flag.Parse()

	args := flag.Args()
	if len(args) != 2 {
		usage()
	}

	src := args[0]
	dst := args[1]

	stat, err := os.Stat(src)
	check(err)
	if !stat.IsDir() {
		usage()
		check(errors.New("src must be a folder name"))
	}

	sf, err := os.Open(src)
	check(err)
	defer sf.Close()

	fis, err := sf.Readdir(0)
	check(err)

	begin := time.Now()
	fn := 0
	log.Println("Begin archiving ...")
	if strings.HasPrefix(dst, ".zip") {
		fn, err = arcZip(fis, src, dst)
		check(err)
	} else {
		fn, err = arcFolder(fis, src, dst)
		check(err)
	}

	log.Println("End.")
	log.Printf("Total files: %v. Processing Time: %v\n", fn, time.Since(begin))
	if strings.HasPrefix(dst, ".zip") {
		log.Println("Deleting the original files ...")
		for _, fi := range fis {
			if fi.IsDir() {
				continue
			}
			err = os.Remove(filepath.Join(src, fi.Name()))
			check(err)
		}
		log.Printf("End. \nTotal Processing Time: %v", time.Since(begin))
	}
}

func arcFolder(fis []os.FileInfo, src, dst string) (int, error) {
	fn := 0
	for _, fi := range fis {
		if fi.IsDir() {
			continue
		}

		//Use os.Rename() for fast remove and keep the file mode information
		oldname := filepath.Join(src, fi.Name())

		newdir := filepath.Join(dst, fi.ModTime().Format(dateFmt(*pattern)))
		err := os.MkdirAll(newdir, os.ModePerm)
		if err != nil {
			return fn, err
		}
		err = os.Rename(oldname, filepath.Join(newdir, fi.Name()))
		if err != nil {
			return fn, err
		}
		fn++
	}
	return fn, nil
}

/*
	stat, err = os.Stat(dst)
	dd := filepath.Dir(dst)
	if err = os.MkdirAll(dd, os.ModePerm); err != nil {
		if !os.IsExist(err) {
			check(err)
		}
	}
*/

// Return processed files
func arcZip(fis []os.FileInfo, src, dst string) (int, error) {
	// TODO: If zipfile exists, append?
	zipfile, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer zipfile.Close()

	zipwriter := zip.NewWriter(zipfile)

	fn := 0
	for _, fi := range fis {
		if fi.IsDir() {
			continue
		}

		w, err := zipwriter.Create(filepath.Join(fi.ModTime().Format(dateFmt(*pattern)), fi.Name()))
		if err != nil {
			return 0, err
		}
		r, err := os.Open(filepath.Join(src, fi.Name()))
		if err != nil {
			return 0, err
		}

		_, err = io.Copy(w, r)
		if err != nil {
			return 0, err
		}

		r.Close()

		fn++
	}
	zipwriter.Close()
	return fn, nil
}
